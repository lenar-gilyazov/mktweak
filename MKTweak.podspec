Pod::Spec.new do |s|
  s.name             = 'MKTweak'
  s.version          = '0.1.0'
  s.summary          = 'A framework for feature flagging, locally and remotely configure and A/B test iOS apps.'

  s.description      = <<-DESC
  MKTweak is a framework for feature flagging, locally and remotely configure and A/B test iOS apps.
                       DESC

  s.homepage         = 'https://github.com/Lenar Gilyazov/MKTweak'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Lenar Gilyazov' => 'lenar.gilyazov@movika.com' }
  s.source           = { :git => 'https://github.com/Lenar Gilyazov/MKTweak.git', :tag => s.version.to_s }

  s.ios.deployment_target   = '11.0'
  s.swift_version           = '5.1'
  s.static_framework = true

  s.source_files = 'Source/MKTweak/**/*'
  
end
