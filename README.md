# MKTweak

[![CI Status](https://img.shields.io/travis/Lenar Gilyazov/MKTweak.svg?style=flat)](https://travis-ci.org/Lenar Gilyazov/MKTweak)
[![Version](https://img.shields.io/cocoapods/v/MKTweak.svg?style=flat)](https://cocoapods.org/pods/MKTweak)
[![License](https://img.shields.io/cocoapods/l/MKTweak.svg?style=flat)](https://cocoapods.org/pods/MKTweak)
[![Platform](https://img.shields.io/cocoapods/p/MKTweak.svg?style=flat)](https://cocoapods.org/pods/MKTweak)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MKTweak is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MKTweak'
```

## Author

Lenar Gilyazov, lenar.gilyazov@movika.com

## License

MKTweak is available under the MIT license. See the LICENSE file for more info.
