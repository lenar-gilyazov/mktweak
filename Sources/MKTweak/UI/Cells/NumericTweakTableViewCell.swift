//
//  NumericTweakTableViewCell.swift
//  
//
//  Created by Lenar Gilyazov on 23.09.2021.
//

import UIKit

class NumericTweakTableViewCell: TextTweakTableViewCell {
  
  override var keyboardType: UIKeyboardType {
    get {
      return .numberPad
    }
  }
  
}
